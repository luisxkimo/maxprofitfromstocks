﻿using System;
using System.Collections.Generic;

namespace Problem1
{
	public class Tools
	{
		public static IEnumerable<int> GenerateStockPrices(int minutes)
		{
			var rand = new Random(Guid.NewGuid().GetHashCode());
			var tmpPrices = new List<int>();

			for (var i = 1; i <= minutes; i++)
			{
				tmpPrices.Add(rand.Next(50, 80));
			}

			return tmpPrices;
		}
	}
}
