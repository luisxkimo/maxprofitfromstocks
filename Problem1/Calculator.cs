﻿using System.Collections.Generic;
using System.Linq;

namespace Problem1
{
	public class Calculator
	{
		public static Dictionary<int, int> Calculate(int[] stocks)
		{
			var cp1 = stocks.ToList();

			var result = new Dictionary<int, int>();

			if (cp1.AssertAllAreEquals())
			{
				return new Dictionary<int, int>
						{
							{0, cp1.First()},
							{1, cp1.First()}
						};
			}

			while (cp1.Any(x => x != 0))
			{
				var max = cp1.MAX();
				var posMax = cp1.IndexOf(max);
				cp1 = ListTools.SetMax(cp1, max);

				var cp2 = cp1.ToList();

				for (var i = 0; i < cp2.Count; i++)
				{
					var min = cp2.MIN();
					var posMin = cp2.IndexOf(min);

					cp2 = ListTools.SetMin(cp2, min);

					if (posMin < posMax)
						return new Dictionary<int, int>
						{
							{posMin, min},
							{posMax, max}
						};
				}
			}

			return result;
		}
	}
}
