﻿using System.Collections.Generic;
using System.Linq;

namespace Problem1
{
	public static class ListExtensions
	{
		public static int MIN(this IList<int> list)
		{
			if (list.Count == 0) return 0;

			var acc = list.Max();
			foreach (var i in list)
			{
				if (i != 0 && i < acc)
					acc = i;
			}

			return acc;
		}

		public static int MAX(this IList<int> list)
		{
			if (list.Count == 0) return 0;

			var acc = list.Min();
			foreach (var i in list)
			{
				if (i != 0 && i > acc)
					acc = i;
			}

			return acc;
		}

		public static bool AssertAllAreEquals(this IEnumerable<int> collection)
		{
			if (collection.Count() < 2) return false;
			else
			{

				var firstItem = collection.First();
				var allEqual = collection.Skip(1)
					.All(s => Equals(firstItem, s));

				return allEqual;
			}
		}
	}
}