﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Problem1
{
	public class Program
	{
		static void Main(string[] args)
		{
			var stockPricesYesterday = Tools.GenerateStockPrices(10).ToArray();

			var result = Calculator.Calculate(stockPricesYesterday);

			Console.WriteLine("Stocks Array Yesterday at 09:30am");
			PrintStocksYesterday(stockPricesYesterday);
			Console.WriteLine("\n");

			Console.WriteLine("Best purchase and sale:\n");

			var timePurchase = GetTimePurchase(result);
			var timeSale = GetTimeSale(result);

			Console.WriteLine("Purchase at {0} with stock value {1}\n",timePurchase.ToShortTimeString(),result.Values.ToArray()[0]);
			Console.WriteLine("Sale at {0} with stock value {1}\n", timeSale.ToShortTimeString(), result.Values.ToArray()[1]);
			Console.ReadLine();
		}

		private static DateTime GetTimePurchase(Dictionary<int, int> result)
		{
			var Now = DateTime.Now;
			var dt = new DateTime(Now.Year, Now.Month, Now.Day, 9, 30, 00);

			var minute = result.Keys.ToArray()[0];
			var value = dt.AddMinutes(minute);
			return value;
		}

		private static DateTime GetTimeSale(Dictionary<int, int> result)
		{
			var Now = DateTime.Now;
			var dt = new DateTime(Now.Year, Now.Month, Now.Day, 9, 30, 00);

			var minute = result.Keys.ToArray()[1];
			var value = dt.AddMinutes(minute);
			return value;
		}

		private static void PrintStocksYesterday(IEnumerable<int> stockPricesYesterday)
		{
			Console.Write("[");
			string acc = string.Empty;
			foreach (var stockValue in stockPricesYesterday)
			{
				acc += stockValue + ", ";
			}
			var result = acc.Substring(0,acc.Count()-2);
			Console.Write(result + "]");
		}
	}
}
