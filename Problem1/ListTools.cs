﻿using System.Collections.Generic;
using System.Linq;

namespace Problem1
{
	public class ListTools
	{
		public static List<int> SetMin(IEnumerable<int> cpStocks, int MIN)
		{
			var cpList = cpStocks.ToList();

			if (cpList.Count == 0) return cpList;

			var min = MIN;
			var position = cpList.IndexOf(min);

			cpList.RemoveAt(position);
			cpList.Insert(position, 0);

			return cpList;
		}

		public static List<int> SetMax(IEnumerable<int> cpStocks, int MAX)
		{
			var cpList = cpStocks.ToList();

			if (cpList.Count == 0) return cpList;

			var max = MAX;
			var position = cpList.IndexOf(max);

			cpList.RemoveAt(position);
			cpList.Insert(position, 0);

			return cpList;
		}
	}
}
