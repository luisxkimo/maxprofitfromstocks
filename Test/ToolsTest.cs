﻿using NUnit.Framework;
using Problem1;

namespace Test
{
	[TestFixture]
	public class ToolsTest
	{

		[Test]
		public void Generate_stocks_get_different_values_always()
		{

			for (var i = 0; i < 10000; i++)
			{
				var stocks1 = Tools.GenerateStockPrices(10);
				var stocks2 = Tools.GenerateStockPrices(10);

				Assert.That(stocks1, Is.Not.EquivalentTo(stocks2));
			}

		}
	}
}
