﻿using System.Collections.Generic;
using NUnit.Framework;
using Problem1;

namespace Test.ListExtensionsTest
{
	[TestFixture]
	public class MINSpecs
	{
		[Test]
		public void Min_empty_list_is_zero()
		{
			var lst = new List<int> { };
			var result = lst.MIN();

			Assert.That(result, Is.EqualTo(0));
		}

		[Test]
		public void Min_with_repeat_items_is_max_item()
		{
			var lst = new List<int> { 1, 1 };
			var result = lst.MIN();

			Assert.That(result, Is.EqualTo(1));
		}

		[Test]
		public void Min_is_zero_with_zero_values()
		{
			var lst = new List<int> { 0, 0 };
			var result = lst.MIN();

			Assert.That(result, Is.EqualTo(0));
		}

		[Test]
		public void Min_return_rigth_min_except_zero_values_in_list()
		{
			var lst = new List<int> { 0, 2, 3, 4 };
			var result = lst.MIN();

			Assert.That(result, Is.EqualTo(2));
		}
	}
}