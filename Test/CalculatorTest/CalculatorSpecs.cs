﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Problem1;

namespace Test.CalculatorTest
{
	[TestFixture]
	public class CalculatorSpecs
	{
		//Test normal

		[Test]
		public void Stocks_array_returns_empty_dictionary()
		{
			var stocks = new int[] { };

			var result = Calculator.Calculate(stocks);

			Assert.That(result.Count, Is.EqualTo(0));
		}

		[Test]
		public void Stocks_with_one_item_returns_empty_dictionary()
		{
			var stocks = new[] { 1 };

			var result = Calculator.Calculate(stocks);

			Assert.That(result.Count, Is.EqualTo(0));
		}

		[Test]
		public void Stocks_with_repeat_values_return_dictionary_with_repeat_values()
		{
			var stocks = new[] { 1, 1, 1 };

			var result = Calculator.Calculate(stocks);

			Assert.That(result.Count, Is.EqualTo(2));

			Assert.That(result.Keys.ToArray()[0], Is.EqualTo(0));
			Assert.That(result.Keys.ToArray()[1], Is.EqualTo(1));

			Assert.That(result.Values, Is.All.EqualTo(1));
		}

		[Test]
		public void Calculator_return_correct_max_and_min_with_positions()
		{
			var stock = new[] { 60, 50, 60 };
			var stock2 = new[] { 79, 52, 63, 58, 78, 61, 62 };
			var stock3 = new[] { 54, 70, 78, 63, 62, 51, 79 };

			var result = Calculator.Calculate(stock);
			var result2 = Calculator.Calculate(stock2);
			var result3 = Calculator.Calculate(stock3);

			AssertData(result.Keys.ToArray(), 1, 2);
			AssertData(result.Values.ToArray(), 50, 60);

			AssertData(result2.Keys.ToArray(), 1, 4);
			AssertData(result2.Values.ToArray(), 52, 78);

			AssertData(result3.Keys.ToArray(), 5, 6);
			AssertData(result3.Values.ToArray(), 51, 79);
		}

		[Test]
		public void Other_example()
		{
			var stock = new[] { 65, 100, 20, 50, 40 };

			var result = Calculator.Calculate(stock);

			AssertData(result.Keys.ToArray(), 0, 1);
			AssertData(result.Values.ToArray(), 65, 100);
		}



		private void AssertData(int[] data, int min, int max)
		{
			Assert.That(data[0], Is.EqualTo(min));
			Assert.That(data[1], Is.EqualTo(max));
		}

	}
}
