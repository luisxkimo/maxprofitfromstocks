using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using Problem1;

namespace Test.ListToolsTest
{
	[TestFixture]
	public class SetMinSpecs
	{
		[Test]
		public void SetMin_in_empty_list_do_nothing()
		{
			var lst = new List<int> { };

			var result = ListTools.SetMin(lst, lst.MIN());

			Assert.AreEqual(result, lst);
		}

		[Test]
		public void SetMin_in_one_list_element_return_new_list_with_value_zero()
		{
			var lst = new List<int> { 10 };

			var result = ListTools.SetMin(lst, lst.MIN());

			Assert.That(result.Count, Is.EqualTo(1));
			Assert.That(result.Single(), Is.EqualTo(0));
		}

		[Test]
		public void SetMin_in_list_with_repeat_element()
		{
			var lst = new List<int> { 10, 10 };

			var result = ListTools.SetMin(lst, lst.MIN());

			Assert.That(result.Count, Is.EqualTo(2));
			Assert.That(result.First(), Is.EqualTo(0));
			Assert.That(result.Last(), Is.EqualTo(10));
		}

		[Test]
		public void SetMin_in_list_set_zero_correct_item()
		{
			var lst = new List<int> { 0, 0, 10, 25, 8, 72, 1, 10, 7, 22 };
			var compareLst = new List<int> { 0, 0, 10, 25, 8, 72, 0, 10, 7, 22 };

			var result = ListTools.SetMin(lst, lst.MIN());

			Assert.That(result.Count, Is.EqualTo(10));
			Assert.That(result, Is.EquivalentTo(compareLst));
		}
	}
}